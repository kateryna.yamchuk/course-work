const array_inputs = [];
	const formValues = [];
	const INPUTS = document.getElementById("inputs");
	const CONFIGURATIONDiv = document.getElementById("configurationSet");
	const CONFIGURATIONForControl = document.getElementById("configuration_options");
	const editDelDiv = document.getElementById("editDelDiv");
	const FORM = document.getElementById("MyForm");
	const SELECT = document.getElementById("select_controls");
	const btnDelete = document.createElement("button");
	const btnEdit = document.createElement("button");
	const addBtn = document.createElement("button");
	const formConfig = document.createElement("button");
	const divForArea = document.createElement("div");
		  divForArea.classList.add("ForAreas");
    const addEditBtn = document.createElement("button");

    let id = 0;
	let textA1; 
	let textA2; 
	let textA3; 
	let newElement;
	let inputname;
	let typeValue;
	let placeHolder;
	let label;
	btnDelete.innerHTML = "Delete";
	btnDelete.addEventListener("click", ()=>{
		array_inputs[SELECT.value].DeleteInput();
	})

	btnEdit.innerHTML = "Edit";
	btnEdit.addEventListener("click", ()=>{
		array_inputs[SELECT.value].EditInput();
	})

	addEditBtn.innerHTML = "Update Form";
	addEditBtn.addEventListener("click", ()=>{
		array_inputs[SELECT.value].UpdateFunc();
	})

	FORM.addEventListener("submit",(e)=>{
		e.preventDefault();
		SubmitAction();
	})

	INPUTS.addEventListener("click", (e)=>{
		CONFIGURATIONDiv.innerHTML = "";
		divForArea.innerHTML ="";
		editDelDiv.innerHTML = "";
		SELECT.value = "new_control";

		inputname = e.target.getAttribute("typeName");
		typeValue = e.target.getAttribute("typeValue");
		placeHolder = e.target.getAttribute("placeHolder");
		
		DrawAddControl();
		
		addBtn.addEventListener("click", AddNewControl);
	})

	SELECT.addEventListener("change",()=> {
		if (SELECT.value != "new_control"){
				CONFIGURATIONDiv.innerHTML = "";
				editDelDiv.appendChild(btnDelete);
				editDelDiv.appendChild(btnEdit);
		}else{
			editDelDiv.innerHTML = "";
			divForArea.innerHTML ="";
			CONFIGURATIONDiv.innerHTML = "";
			DrawAddControl();
			addBtn.addEventListener("click",AddNewControl);
		}
	})

	class Input {
		constructor(inputname,type,name,value, innertext){
			this.inputname = inputname;
			this.type = type;
			this.name = name;
			this.value = value;
			this.id = id;
			this.innertext = innertext;
			this.DeleteInput = this.DeleteInput.bind(this);
			this.EditInput = this.EditInput.bind(this);
			this.UpdateFunc = this.UpdateFunc.bind(this);
		}
	
		DeleteInput() {
	  		FORM.innerHTML = "";
	  		editDelDiv.removeChild(btnEdit);
      		delete array_inputs[this.id];

      		for (let i=0; i<SELECT.length; i++){
  				if (SELECT.options[i].value == this.id )
     			SELECT.remove(i);
  	   		}
     
      		if (SELECT.value == "new_control"){
      			editDelDiv.removeChild(btnDelete);
       		} 
      		array_inputs.forEach( (item) => {
      			DrawOnForm(item.type,item);
      		})
		}

		EditInput() {
			CONFIGURATIONDiv.appendChild(divForArea);
				label.innerHTML = this.inputname;
				CONFIGURATIONDiv.appendChild(label);

				CONFIGURATIONDiv.insertBefore(label,divForArea); 
				textA1.value = this.name;
				divForArea.appendChild(textA1);

			    textA2.style.marginLeft = "5px";
				textA2.value = this.value;
				divForArea.appendChild(textA2);

			    textA3.style.marginLeft = "5px";
				textA3.value = this.innertext;
				divForArea.appendChild(textA3);	
			
				CONFIGURATIONDiv.appendChild(addEditBtn);

				editDelDiv.removeChild(btnEdit);
			    editDelDiv.removeChild(btnDelete);

			}

		UpdateFunc() {
			if (textA1.value == ""){
				alert("Please, fill Name of input!");
			}else{

				for (let i=0; i<SELECT.length; i++){
	  				if (SELECT.options[i].value == this.id ){
	     				SELECT.options[i].innerHTML = textA1.value;
	     			}
	  	   		}
				FORM.innerHTML = "";
				array_inputs[this.id].name = textA1.value;
				array_inputs[this.id].value = textA2.value;
				array_inputs[this.id].innertext = textA3.value;

				array_inputs.forEach( (item) => {
					DrawOnForm(item.type,item);
	      		})
			}

		}

	}

	const DrawAddControl = () => {

		label = document.createElement("label");
		label.innerHTML = inputname;
		CONFIGURATIONDiv.appendChild(label);

		CONFIGURATIONDiv.appendChild(divForArea);
		textA1 = document.createElement("textarea"); 
			textA1.setAttribute("placeholder","name");
			divForArea.appendChild(textA1);

		textA2 = document.createElement("textarea");
		    textA2.style.marginLeft = "5px";
			textA2.setAttribute("placeholder",placeHolder);
			divForArea.appendChild(textA2);

		textA3 = document.createElement("textarea");
		    textA3.style.marginLeft = "5px";
			textA3.setAttribute("placeholder","Label (required for radio and checkbox)");
			divForArea.appendChild(textA3);	
		
			addBtn.innerHTML = "Add to Form";
			CONFIGURATIONDiv.appendChild(addBtn);
	}

	const AddNewControl = () =>{
		if (textA1.value == ""){
			alert("Please, fill Name of input!");
		}else{
			let f = true;
			if (typeValue == "submit"){
				array_inputs.forEach( (item) =>{
					if (item.type == "submit"){
						f = false
					}
				})
		    }
			if (f == true) {
				array_inputs[id] = new Input(inputname,typeValue,textA1.value,textA2.value,textA3.value);
				DrawOnForm(typeValue,array_inputs[id]);
				
				textA1.value = "";
				textA2.value = "";
				textA3.value = "";

				option = document.createElement("option");
					SELECT.appendChild(option);
					option.innerHTML=array_inputs[id].name;
					option.value=array_inputs[id].id;

		

				id++;
			}else{
	   			alert("Submit alredy exists!");
			}
  		}
	}

	const SubmitAction = () => {
		console.log("Form Data:");
		for (let i=0; i < FORM.elements.length; i++){
			if((FORM.elements[i].type !== "submit" && FORM.elements[i].type !== "reset" && FORM.elements[i].type !== "checkbox" && FORM.elements[i].type !== "radio") || ((FORM.elements[i].type == "checkbox" || FORM.elements[i].type == "radio") && FORM.elements[i].checked) ){
			console.log(FORM.elements[i].name+" : "+FORM.elements[i].value);
			}
		}
	}

	const DrawOnForm = (typeValue,input) => {
		if (typeValue == "label"){
			newElement = document.createElement("label");
			newElement.setAttribute("name",input.name);
			newElement.innerHTML = input.innertext;
			newElement.setAttribute("id",input.id);
			FORM.appendChild(newElement);
			FORM.appendChild(document.createElement("br"));
		} 

		else {
			newElement = document.createElement("input");
			newElement.setAttribute("type",input.type);
			newElement.setAttribute("name",input.name);
			newElement.setAttribute("value",input.value);
			newElement.setAttribute("id",input.id);

			let label = document.createElement("label");
			label.setAttribute("for",id);
			label.innerHTML = input.innertext;

			if (typeValue == "radio"){
			   	let divForRad = document.createElement("div");
			   	FORM.appendChild(divForRad);
			   	divForRad.appendChild(newElement);
			   	divForRad.appendChild(label);

			}else if (typeValue == "checkbox"){
				FORM.appendChild(label);
				label.appendChild(newElement);

			}else{
				FORM.appendChild(label);
				FORM.appendChild(newElement);
			}
			FORM.appendChild(document.createElement("br"));
		}
	}